package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.util.AttributeSet
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView
import org.w3c.dom.Attr

class ActivityOne : Activity() {

    // lifecycle counts
    //TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called and log the info
    private var createCount: Int = 0
    private var startCount: Int = 0
    private var resumeCount: Int = 0
    private var pauseCount: Int = 0
    private var stopCount: Int = 0
    private var restartCount: Int = 0
    private var destroyCount: Int = 0

    private lateinit var tvcreate: TextView
    private lateinit var tvstart: TextView
    private lateinit var tvresume: TextView
    private lateinit var tvpause: TextView
    private lateinit var tvstop: TextView
    private lateinit var tvrestart: TextView
    private lateinit var tvdestroy: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        tvcreate = findViewById(R.id.create) as TextView
        tvstart = findViewById(R.id.start) as TextView
        tvresume = findViewById(R.id.resume) as TextView
        tvpause = findViewById(R.id.pause) as TextView
        tvstop = findViewById(R.id.stop) as TextView
        tvrestart = findViewById(R.id.restart) as TextView
        tvdestroy = findViewById(R.id.destroy) as TextView


        //Log cat print out
        Log.i(TAG, "onCreate called")

        //TODO: update the appropriate count variable & update the view
        tvcreate.setText(resources.getString(R.string.onCreate) + ++createCount)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    // lifecycle callback overrides

    public override fun onStart() {
        super.onStart()

        //Log cat print out
        Log.i(TAG, "onStart called")

        //TODO:  update the appropriate count variable & update the view
        tvstart.setText(resources.getString(R.string.onStart) + ++startCount)
    }

    // TODO: implement 5 missing lifecycle callback methods with Logging and
    // TODO: increment the counter variables' values when their corresponding lifecycle methods are called and log the info

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    override fun onResume() {
        super.onResume()

        //Log cat print out
        Log.i(TAG, "onResume called")

        //TODO:  update the appropriate count variable & update the view
        tvresume.setText(resources.getString(R.string.onResume) + ++resumeCount)
    }

    override fun onPause() {
        super.onPause()

        //Log cat print out
        Log.i(TAG, "onPause called")

        //TODO:  update the appropriate count variable & update the view
        tvpause.setText(resources.getString(R.string.onPause) + ++pauseCount)
    }

    override fun onStop() {
        super.onStop()

        //Log cat print out
        Log.i(TAG, "onStop called")

        //TODO:  update the appropriate count variable & update the view
        tvstop.setText(resources.getString(R.string.onStop) + ++stopCount)
    }

    override fun onRestart() {
        super.onRestart()

        //Log cat print out
        Log.i(TAG, "onRestart called")

        //TODO:  update the appropriate count variable & update the view
        tvrestart.setText(resources.getString(R.string.onRestart) + ++restartCount)
    }

    override fun onDestroy() {
        super.onDestroy()

        //Log cat print out
        Log.i(TAG, "onDestroy called")

        //TODO:  update the appropriate count variable & update the view
        tvdestroy.setText(resources.getString(R.string.onDestroy) + ++destroyCount)
    }

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        //TODO:  save state information with a collection of key-value pairs & save all  count variables
    }


    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityOne"
    }


}
